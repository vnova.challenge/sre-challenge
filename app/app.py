from flask import Flask, jsonify, request, send_file
from flask import render_template, redirect, url_for, request
from werkzeug import secure_filename
import config as cfg
import boto3, os, time
from boto3.s3.transfer import TransferConfig, S3Transfer
from flask_swagger import swagger
app = Flask(__name__)


app = Flask("python_s3")
app.debug = True

FILE ="./s3transfer/requirements.txt"
OUTFILE = "./weather/requirements.txt"
bucket ="dev-vnova-bucket"
key = "requirements.txt"

# upload api
@app.route("/upload", methods=['GET', 'POST'])
def upload():
    client = boto3.client('s3')
    transfer = S3Transfer(client)
    # Upload /tmp/myfile to s3://bucket/key
    transfer.upload_file(FILE, bucket, key)
    return jsonify({'success': 1})



# download api
@app.route("/download/requirements.txt", methods=['GET'])
def download():
    client = boto3.client('s3')
    transfer = S3Transfer(client)
    transfer.download_file(bucket,key, OUTFILE)
    return jsonify({'success': 1})



# Swagger Doccument for API
@app.route('/docs')
def spec():
    swag = swagger(app)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "Python S3 API"
    swag['basePath'] = "/"
    return jsonify(swag)

if __name__ == '__main__':
    app.debug=True
    app.run()
