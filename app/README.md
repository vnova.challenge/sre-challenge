
- Setup the flask environment:

virtualenv -p /usr/local/bin/python3 env
. env/bin/activate

pip install flask
pip install config
pip install boto3
pip install flask_swagger


Run application with the following:
python3 app.py
~
~
