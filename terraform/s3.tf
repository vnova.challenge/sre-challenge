

module "s3_bucket" {
  source = "git::https://github.com/cloudposse/terraform-aws-s3-bucket.git?ref=master"

  enabled                      = true
  user_enabled                 = true
  region                       = var.region
#  namespace                    = var.namespace
  stage                        = var.stage
  name                         = var.name
  acl                          = var.acl
  force_destroy                = var.force_destroy
  versioning_enabled           = var.versioning_enabled
#  allow_encrypted_uploads_only = var.allow_encrypted_uploads_only
  allowed_bucket_actions       = var.allowed_bucket_actions
  policy                       = <<POLICY
{
  "Id": "Policy1572393531612",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1572393524878",
      "Action": ["s3:Get*",
                "s3:List*"
               ],
      "Effect": "Allow",
      "Resource": "arn:aws:s3:::dev-vnova-bucket/*",
      "Principal": {
        "AWS": [
          "arn:aws:iam::248580631121:root"
        ]
      }
    }
  ]
}
POLICY
}


