resource "aws_vpc" "default" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames = true
}




resource "aws_internet_gateway" "default" {
    vpc_id = "${aws_vpc.default.id}"
}


resource "aws_subnet" "eu-west-1a-public" {
    vpc_id = "${aws_vpc.default.id}"
    cidr_block = "${var.public_subnet_cidr}"
    availability_zone = "eu-west-1a"

  tags = {
    Name = "env: dev, author: willsc"
  }
}



resource "aws_route_table" "eu-west-1a-public" {
    vpc_id = "${aws_vpc.default.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.default.id}"
    }

   tags = {
    Name = "env: dev, author: willsc"
  }

}


resource "aws_security_group" "web" {
    name = "vpc_web"
    description = "Allow incoming HTTP connections."
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"
   
   tags = {
    Name = "env: dev, author: willsc"
  }
}




resource "aws_security_group" "bastion" {
    name = "vpc_bastion"
    description = "Allow ssh connections"
    ingress {
        from_port = 22 
        to_port = 22 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

   tags = {
    Name = "env: dev, author: willsc"
  }
    
}





resource "aws_instance" "bastion" {
  ami                          = data.aws_ami.amazon_linux.id
  instance_type                = "t2.micro"
  key_name                     = "ec2-key"
  monitoring                   = true
  vpc_security_group_ids       = ["${aws_security_group.bastion.id}"]
  subnet_id                    = "${aws_subnet.eu-west-1a-public.id}"
  associate_public_ip_address  = true


  tags = {
    Name = "env: dev, author: willsc"
  }

}

resource "aws_instance" "web" {
  ami                          = "ami-0d9443ac74ce2ee5e"
  instance_type                = "t2.micro"
  key_name                     = "ec2-key"
  monitoring                   = true
  subnet_id                    = "${aws_subnet.eu-west-1a-public.id}"
  vpc_security_group_ids       = ["${aws_security_group.web.id}"]
  associate_public_ip_address  = true
  
  tags = {
    Name = "env: dev, author: willsc"
  }


}
