variable "security_group_id" {
   default = "sg-0deaf416e72223828"

}



data "aws_security_group" "selected" {
  id = "${var.security_group_id}"
}




resource "aws_security_group" "packer_build" {
  name        = "packer-build"
  description = "Security Group for Packer Builds"
  vpc_id      = "${data.aws_security_group.selected.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "packer-build"
  }
}
