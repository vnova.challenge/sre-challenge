

# Declare the data source
data "aws_availability_zones" "available" {
  state = "available"
}



module "vpc" {
  name    = "foo-vpc"
  source  = "terraform-aws-modules/vpc/aws"

  cidr = "10.9.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  azs = [
    "${data.aws_availability_zones.available.names[0]}",
    "${data.aws_availability_zones.available.names[1]}",
  ]

  private_subnets  = var.private_subnets
  public_subnets   = var.public_subnets
  database_subnets = var.database_subnets

  create_database_subnet_group = false
}

