variable "access_key" {
   default = "AKIAJIZ3DUF3BTMT52NQ"
}

variable "secret_key" {
   default = "45pSQ0KILebAkvm4pNJbFemcF67KDW3oIUvkGD3w"
}

variable "region" {
  default = "eu-west-1"
}
variable "aws_availability_zones" {
   default  = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}
variable "vpc_cidr" {
  default = "10.9.0.0/16"
}


variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.9.0.0/24"
}

variable "private_subnets" {
  default = ["10.9.0.0/24", "10.9.1.0/24"]
}
variable "public_subnets" {
  default = ["10.9.10.0/24", "10.9.11.0/24"]
}
variable "database_subnets" {
  default = ["10.9.20.0/24", "10.9.21.0/24"]
}
variable "sshpubkey_file" {
  default = "/Users/cwills/.ssh/id_rsa.pub"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "s3-bucket" {
   default = "vnova-bucket"

}


#variable "namespace" {
#   default  = "foo"
#}

variable "stage" {
   default  = "dev"
}

variable "name" {
   default  = "vnova-bucket"
}

variable "acl" {
   default  = "private"
}


variable "force_destroy" {
   default  = true
}

variable "versioning_enabled" {
   default = false
}


variable "allow_encrypted_uploads_only" {
   default = true
}

variable "allowed_bucket_actions" {
   default  = ["s3:PutObject", "s3:PutObjectAcl", "s3:GetObject", "s3:GetObjectAcl", "s3:DeleteObject", "s3:ListBucket", "s3:ListBucketMultipartUploads", "s3:GetBucketLocation", "s3:AbortMultipartUpload"]
}
