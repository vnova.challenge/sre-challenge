#!/bin/bash


set -ex
export DEBIAN_FRONTEND=noninteractive
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get -y -q  install cowsay
sudo apt-get -y install net-tools
sudo apt-get -y install htop 
sudo apt-get -y install tmux 
sudo apt-get -y install python 
sudo apt-get -y install ansible

# Set the file limits 

sudo bash -c 'echo "root soft nofile 2048" >> /etc/security/limits.conf'
sudo bash -c 'echo "root soft nofile 2097152" >> /etc/security/limits.conf'


# Turn off port forwarding

sudo bash -c 'iptables -P FORWARD DROP'
sudo bash -c '/sbin/iptables-save > /etc/iptables.rules'


